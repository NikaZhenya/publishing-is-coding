# Blog del perro

[![pipeline status](https://gitlab.com/perritotuerto/codigo/perrotuerto.blog/badges/no-masters/pipeline.svg)](https://gitlab.com/NikaZhenya/perrotuerto.blog/-/commits/no-masters)

### License

The code is under [GPLv3](https://www.gnu.org/licenses/gpl.html).

### TODO

* RSS feed from JSON
* Reading options (font family, font size...)
* URL analizer for adding or filtrer tags (https://.../?tag=a&tag=b)
